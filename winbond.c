#include "winbond.h"

//*****************************************************************************
//
// The UART initialization for console printing
//
//*****************************************************************************
void init_uart(void) {
    // Enable the GPIO Peripheral used by the UART.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    // Enable UART0
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

    // Configure GPIO Pins for UART mode.
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    // Use the internal 16MHz oscillator as the UART clock source.
    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);

    // Initialize the UART for console I/O.
    UARTStdioConfig(0, 9600, 16000000);
}

//*****************************************************************************
//
// Helper function that formats 256 bytes of data into an easier to read grid
//
//*****************************************************************************
void print_page_bytes(uint8_t *page_buffer) {
    uint8_t i, j;
    for (i = 0; i < 16; i++) {
        for (j = 0; j < 16; j++) {
            UARTprintf("\%02x ", page_buffer[i * 16 + j]);
        }
        UARTprintf("\n");
    }
}

//*****************************************************************************
//
// Chip select
//
//*****************************************************************************
void chip_select(uint8_t val) {
    GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_3, val);
}

//*****************************************************************************
//
// Ping busy bit from Flash status register
//
//*****************************************************************************
void not_busy(void) {
    chip_select(~GPIO_PIN_3);    // /Chip select low
    byte_transfer(WB_READ_STATUS_REG_1);
    while (byte_transfer(0x00) & 1);
    chip_select(GPIO_PIN_3);     // /Chip select high
}

//*****************************************************************************
//
// Read flash device info
//
//*****************************************************************************
void read_id(void) {
    uint8_t man_id, dev_id;

    UARTprintf("Acquiring Flash Info...\n");

    chip_select(~GPIO_PIN_3);    // /Chip select low

    byte_transfer(WB_READ_ID);
    byte_transfer(0x00);
    byte_transfer(0x00);
    byte_transfer(0x00);
    man_id = byte_transfer(0x00);
    dev_id = byte_transfer(0x00);

    chip_select(GPIO_PIN_3);     // /Chip select high

    UARTprintf("Manufacturing ID: \%2x\n", man_id);
    UARTprintf("Device ID: \%2x\n", dev_id);
}

//*****************************************************************************
//
// Read flash based on given starting address and number of bytes to read
//
//*****************************************************************************
void my_spi_read(uint32_t addr, uint32_t num_byte, uint8_t *dst) {
//    UARTprintf("Reading \%0d Bytes of Data from Starting Address 0x\%02x",
//               num_byte, (addr >> 16) & 0xff);
//    UARTprintf("\%02x", (addr >> 8) & 0xff);
//    UARTprintf("\%02x\n", addr & 0xff);

    chip_select(~GPIO_PIN_3);    // /Chip select low

    byte_transfer(WB_READ_DATA);
    byte_transfer((addr >> 16) & 0xFF);
    byte_transfer((addr >> 8) & 0xFF);
    byte_transfer((addr >> 0) & 0xFF);

    int i;
    for (i = 0; i < num_byte; i++) {
        dst[i] = byte_transfer(0x00);
    }

    chip_select(GPIO_PIN_3);     // /Chip select high
    not_busy();
//    print_page_bytes(dst);
//    UARTprintf("Done\n\n");
}

//*****************************************************************************
//
// Helper function to my_spi_write
//
//*****************************************************************************
void write_page(uint32_t addr, uint32_t num_byte, uint8_t *src) {
    chip_select(~GPIO_PIN_3);    // /Chip select low

    byte_transfer(WB_WRITE_ENABLE);

    chip_select(GPIO_PIN_3);     // /Chip select high
    chip_select(~GPIO_PIN_3);    // /Chip select low

    byte_transfer(WB_PAGE_PROGRAM);
    byte_transfer((addr >> 16) & 0xFF);
    byte_transfer((addr >>  8) & 0xFF);
    byte_transfer((addr >>  0) & 0xFF);

    int i;
    for (i = 0; i < num_byte; i++) {
        byte_transfer(src[i]);
    }

    chip_select(GPIO_PIN_3);     // /Chip select high
    not_busy();
}

//*****************************************************************************
//
// Write flash base on given starting address, number of bytes and data
//
//*****************************************************************************
void my_spi_write(uint32_t addr, uint32_t num_byte, uint8_t *src) {
//    printf("Writing %n Bytes of Data from Starting Address 0x%2",
//           num_byte, (addr >> 16));
//    printf("%2", (addr >> 8));
//    printf("%2\r\n\r\n", addr);

    uint8_t num_page = 0, num_single = 0, not_page_aligned = 0, count = 0;

    // check for page alignment
    not_page_aligned = addr % WB_PAGE_SIZE;

    // count of free bytes left between starting address and next closest
    // page bound(upper bound)
    count = WB_PAGE_SIZE - not_page_aligned;

    // number of full pages
    num_page = num_byte / WB_PAGE_SIZE;

    // number of single bytes remaining (too small to form a page, <256B)
    num_single = num_byte % WB_PAGE_SIZE;

    // Page is aligned
    if (not_page_aligned == 0) {
        while (num_page--) {
            write_page(addr, WB_PAGE_SIZE, src);
            addr += WB_PAGE_SIZE;
            src += WB_PAGE_SIZE;
        }

        // if total page is not a whole number,
        // this takes care of the remainings
        if (num_single > 0) {
            write_page(addr, num_single, src);
        }
        // }
    // page not aligned
    } else {
        // if the count of free bytes between starting addr and next closest
        // page bound is too small
        if (num_byte > count) {
            // write the head(excess data bytes) off to make the rest page
            // aligned
            write_page(addr, count, src);

            // updating address and source buffer index
            addr += count;
            src += count;

            // resetting data byte information
            num_byte -= count;
            num_page = num_byte / WB_PAGE_SIZE;
            num_single = num_byte % WB_PAGE_SIZE;

            while (num_page--) {
                write_page(addr, WB_PAGE_SIZE, src);
                addr += WB_PAGE_SIZE;
                src += WB_PAGE_SIZE;
            }

            if (num_single > 0) {
                write_page(addr, num_single, src);
            }

        } else {
            write_page(addr, num_byte, src);
        }
    }

//    printf("Done\r\n\r\n");
//    printf("Wrote %n Bytes of Data from Starting Address 0x%x",
//           num_byte, (addr >> 16));
//    printf("%x", (addr >> 8));
//    printf("%x\r\n\r\n", addr);
}

//*****************************************************************************
//
// Helper function for my_spi_erase
//
//*****************************************************************************
void block_erase_64KB(uint32_t addr) {
    chip_select(~GPIO_PIN_3);    // /Chip select low

    byte_transfer(WB_WRITE_ENABLE);

    chip_select(GPIO_PIN_3);     // /Chip select highs
    chip_select(~GPIO_PIN_3);    // /Chip select low
    byte_transfer(WB_BLOCK_ERASE_64);

    byte_transfer((addr >> 16) & 0xFF);
    byte_transfer((addr >>  8) & 0xFF);
    byte_transfer((addr >>  0) & 0xFF);

    chip_select(GPIO_PIN_3);     // /Chip select high

    // Check busy bit
    not_busy();
}

//*****************************************************************************
//
// Erase a block(64kB) given starting address
//
//*****************************************************************************
void my_spi_erase(uint32_t addr, uint32_t num_byte) {
//    printf("Erasing %n Bytes of Data from Starting Address 0x%2",
//           num_byte, (addr >> 16));
//    printf("%2", (addr >> 8));
//    printf("%2\r\n", addr);

    uint8_t num_block = 0, num_single = 0;

    num_block = num_byte / WB_BLOCK_SIZE_64;  // number of full blocks
    num_single = num_byte % WB_BLOCK_SIZE_64;  // number of leftovers

    while (num_block--) {
        block_erase_64KB(addr);
        addr += WB_BLOCK_SIZE_64;
        num_byte -= WB_BLOCK_SIZE_64;
    }

    if (num_single > 0) {
        block_erase_64KB(addr);
    }

//    printf("Done\r\n\r\n");
//    printf("Erased %n Bytes of Data from Starting Address 0x%x",
//           num_byte, (addr >> 16));
//    printf("%x", (addr >> 8));
//    printf("%x\r\n", addr);
}

//*****************************************************************************
//
// Erase the whole chip
//
//*****************************************************************************
void chip_erase(void) {
    chip_select(~GPIO_PIN_3);    // /Chip select low
    byte_transfer(WB_WRITE_ENABLE);
    chip_select(GPIO_PIN_3);     // /Chip select high
    chip_select(~GPIO_PIN_3);    // /Chip select low
    byte_transfer(WB_CHIP_ERASE);
    chip_select(GPIO_PIN_3);     // /Chip select high
    UARTprintf("Erasing Chip ...\n");
    not_busy();
    UARTprintf("Chip has been successfully erased!\n\n");
}

//*****************************************************************************
//
// Main
//
//*****************************************************************************
int main(void) {
    SysCtlClockSet(SYSCTL_SYSDIV_2_5 |
                   SYSCTL_USE_PLL |
                   SYSCTL_OSC_MAIN |
                   SYSCTL_XTAL_16MHZ);

    // Initialize peripherals
    init_uart();
    init_spi();

    read_id();

//    chip_erase();

//    block_erase_64KB(0x3F0000);
//    uint8_t buf[256];
//    my_spi_read(0x000100, 256, buf);
//    my_spi_read(0x3FFF00, 256, buf);
//    uint8_t write_data[] = {0x12, 0x02, 0x03, 0x04};
//    write_page(0x3FFF00, 4, write_data);
//    my_spi_read(0x3FFF00, 256, buf);

    return 0;
}
