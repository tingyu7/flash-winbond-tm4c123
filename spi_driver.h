#ifndef SPI_DRIVER_H_
#define SPI_DRIVER_H_

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_ssi.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "driverlib/ssi.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "inc/tm4c123gh6pm.h"

//*****************************************************************************
//
// Function prototype
//
//*****************************************************************************
void init_spi(void);
uint8_t byte_transfer(uint8_t data);
void SSI0IntHandler(void);

#endif /* SPI_DRIVER_H_ */
